// ## Задание

// Добавить в домашнее задание HTML/CSS №4 (Flex/Grid) различные эффекты с использованием jQuery

// #### Технические требования:
// - Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы.
// - При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
// - Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
// - Добавить под одной из секций кнопку, которая будет выполнять функцию `slideToggle()` (прятать и показывать по клику) для данной секции.

// #### Литература:
// - [Якоря](http://htmlbook.ru/samhtml/yakorya)
// - [jQuery для начинающих](http://anton.shevchuk.name/javascript/jquery-for-beginners/)
// - [Стандартные анимации jQuery](http://jquery.page2page.ru/index.php5/%D0%AD%D1%84%D1%84%D0%B5%D0%BA%D1%82%D1%8B)

// Yakorya

$(".menu").on("click", "a", function (e) {
    e.preventDefault();
    const id = $(this).attr('href')
    const top = $(id).offset().top;
    $('body,html').animate({ scrollTop: top }, 3000);
});

// slide_toggle

$('.slide_toggle').on('click', function (e) {
    e.preventDefault();
    $('.enrgym-card').slideToggle("slow");

})

// UP button

$(window).scroll(function (e) {
    if ($(window).scrollTop() >= 300) {
        $('#button').css('opacity', '1')
    } else {
        $('#button').css('opacity', '0')
    }
});

$('#button').on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});
