//  Теоретический вопрос

// 1. Описать своими словами для чего вообще нужны функции в программировании.
// 2. Описать своими словами, зачем в функцию передавать аргумент.

// Задание

// Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. 
// Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

//  Технические требования:
// - Считать с помощью модального окна браузера два числа.
// - Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено `+`, `-`, `*`, `/`.
// - Создать функцию, в которую передать два значения и операцию.
// - Вывести в консоль результат выполнения функции.

//  Необязательное задание продвинутой сложности:
// - После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа,
//- спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

//  Литература:
// - [Функции - основы](https://learn.javascript.ru/function-basics)


let number1 = +prompt('Enter number!');
while (Number.isNaN(number1) || number1 === null || number1 === '') {
    number1 = +prompt('Enter number!');
}

let operant = prompt('+, -, *, /');

let number2 = +prompt('Enter number!');
while (Number.isNaN(number2) || number2 === null || number2 === '') {
    number2 = +prompt('Enter number!');
}

const result = calculate(number1, operant, number2)
function calculate(number1, operant, number2) {
    switch (operant) {
        case "+":
            return number1 + number2;
        case "-":
            return number1 - number2;
        case "*":
            return number1 * number2;
        case "/":
            return number1 / number2;
        default:
            break;
    }
}
alert ('View The Result Of Miscalculations In The Console!');
console.log(`${number1} ${operant} ${number2} = ${result}`);
