//  Теоретический вопрос

// 1. Опишите своими словами, что такое метод обьекта
//  методы обекта позволяют нам узнать его значение, длинну, свойство, ключи 


//  Задание

// Реализовать функцию для создания объекта "пользователь". Задача должна быть реализована на языке javascript,
// без использования фреймворков и сторонник библиотек (типа Jquery).

// Технические требования:
// - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
// - Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя, 
//   соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
// - Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`.
//   Вывести в консоль результат выполнения функции.

// Необязательное задание продвинутой сложности:
// - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. 
//  Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.

//  Литература:
// - [Объекты как ассоциативные массивы](https://learn.javascript.ru/object)
// - [Object.defineProperty()](https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)


function createNewUser(){
    this.firstName = prompt('Enter first Name!');
    this.lastName = prompt('Enter last Name!');
    this.getLogin = function(){
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
}
let newUser = new createNewUser();
let login = newUser.getLogin();

console.log(newUser);
console.log(`Для пользователя ${newUser.firstName}, ${newUser.lastName} создано логин  ${login} !`);


