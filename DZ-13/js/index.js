// ## Задание

// Реализовать возможность смены цветовой темы сайта пользователем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - Взять любое готовое домашнее задание по HTML/CSS.
// - Добавить на макете кнопку "Сменить тему".
// - При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// - Выбранная тема должна сохраняться и после перезагрузки страницы

// #### Литература:
// - [LocalStorage на пальцах](https://tproger.ru/articles/localstorage/)



const buttonChangeTheme = document.querySelector('.inverse-style')
const themeCssSelector = buttonChangeTheme.dataset.target;
const themeCssLink = document.querySelector(themeCssSelector);

buttonChangeTheme, addEventListener('click', function () {
    if (themeCssLink.href.includes("css/main.css")) {
        const newHref = themeCssLink.href.replace("css/main.css", "css/main_white.css");
        themeCssLink.href = newHref;
        localStorage.setItem('themeSwitcher', 'white')
    } else {
        const newHref = themeCssLink.href.replace("css/main_white.css", "css/main.css");
        themeCssLink.href = newHref;
        localStorage.setItem('themeSwitcher', 'main')
    }
})
if (localStorage.getItem('themeSwitcher') === 'white') {
    themeCssLink.setAttribute('href', 'css/main_white.css')
}
else {
    themeCssLink.setAttribute('href', 'css/main.css')
}
