// ## Задание

// Реализовать переключение вкладок (табы) на чистом Javascript.

// #### Технические требования:
// - В папке `tabs` лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки. 
// - Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// - Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

// #### Литература:
// - [HTMLElement.dataset](https://developer.mozilla.org/ru/docs/Web/API/HTMLElement/dataset)



const tabItem = document.querySelectorAll('.tab .tab_title');

tabItem.forEach(elem => elem.addEventListener('click', function () {

    const tabContainer = this.closest('.tab');
    const prevActiveTab = tabContainer.querySelector('.tab_title.active');
    prevActiveTab.classList.remove('active');
    this.classList.add('active');

    const Items = tabContainer.querySelectorAll('.tab_title');
    for (let i = 0; i < Items.length; i++) {
        if (Items[i] === this) {
            index = i;
            break;
        }
    }
    const itemActiveRemove = document.querySelector('.item.active');
    itemActiveRemove.classList.remove('active');

    const activeItemAdd = document.querySelectorAll('.item')[index];
    activeItemAdd.classList.add('active');
}))
