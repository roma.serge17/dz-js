// ## Задание

// Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования фреймворков
// и сторонник библиотек (типа Jquery).

// #### Технические требования:
// - В файле `index.html` лежит разметка для двух полей ввода пароля.
// - По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет 
//   свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
// - Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// - Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// - По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// - Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
// - Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые значения`
// - После нажатия на кнопку страница не должна перезагружаться
// - Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.


const showPasswordHide = document.querySelectorAll('.fas.icon-password');
const password = document.querySelector('.password-form');
const errorPassword = document.querySelector('.error');

showPasswordHide.forEach(elem => elem.addEventListener('click', function () {
    const input = elem.previousElementSibling;

    if (this.className === 'fas fa-eye icon-password') {
        this.className = 'fas fa-eye-slash icon-password';
        input.setAttribute('type', 'text');
    }
    else {
        this.className = 'fas fa-eye icon-password';
        input.setAttribute('type', 'password');
    }
}))

password.addEventListener('submit', function () {
    const firstPassword = document.querySelector('.input-field.first_input');
    const secondPassword = document.querySelector('.input-field.second_input');

    if (firstPassword.value === secondPassword.value) {
        errorPassword.classList.remove('active');
        alert('You are welcome ! ');
    }
    else {
        errorPassword.classList.add('active');

    }

})